#!/usr/bin/python3

import argparse, fileinput, re, sys
from collections import defaultdict

INSTART = 0
INFEATURE = 1
INSEQ = 2

class Anno:
    def __init__(self, annostart, annoend, refname):
        self.annostart = annostart
        self.annoend = annoend
        self.protein_id = None
        self.gene = None
        self.product = None
        self.uniprot = None
        self.refname = refname
        self.locus_tag = None

    def has_some_name(self):
        return self.get_some_name() is not None

    def get_product(self):
        if self.product is None:
            return ""
        else:
            return self.product

    def get_uniprot(self):
        if self.uniprot is None:
            return ""
        else:
            return self.uniprot

    def get_locus_tag(self):
        if self.locus_tag is None:
            return ""
        else:
            return self.locus_tag

    def get_protein_id(self):
        if self.protein_id is None:
            return ""
        else:
            return self.protein_id

    def get_gene_name(self):
        if self.gene is None:
            return ""
        else:
            return self.gene

    def get_some_name(self):
        if self.protein_id is not None:
            return self.protein_id
        elif self.gene is not None:
            return "NOID:" + self.gene
        return None

def extract_name(infile, verbose):
    definition="UNKNOWN"
    accession="UNKNOWN"
    lines = 0
    for line in infile:
        if line.startswith("DEFINITION"):
            definition = " ".join(line.split(" ")[1:]).strip().replace(" ", "_")
        if line.startswith("ACCESSION"):
            accession = " ".join(line.split(" ")[1:]).strip().replace(" ", "_")
        lines += 1
        if accession != "UNKNOWN" and definition != "UNKNOWN":
            break
        if lines > 4:
            if args.verbose:
                print("WARNING: Did not find accession and definition in the first 4 lines.")
            break
    return accession + " " + definition

def extract_annotations(infile, annotype, verbose, refname):
    from Bio import SeqIO
    genome = SeqIO.read(infile.name, 'genbank')

    res = []
    curranno = None
    numanno = 0

    for feature in genome.features:
        if feature.type == 'CDS':
            curranno = Anno(feature.location.start.position, feature.location.end.position, refname)
            if 'gene' in feature.qualifiers:
                curranno.gene = feature.qualifiers['gene'][0]
            if 'protein_id' in feature.qualifiers:
                curranno.protein_id = feature.qualifiers['protein_id'][0]
            if 'product' in feature.qualifiers:
                curranno.product = feature.qualifiers['product'][0]
            if 'locus_tag' in feature.qualifiers:
                curranno.locus_tag = feature.qualifiers['locus_tag'][0]
            if 'db_xref' in feature.qualifiers:
                cache = [x for x in feature.qualifiers['db_xref'] if 'UniProt' in x]
                if len(cache) != 0:
                    curranno.uniprot = cache[0].split(':')[1]
            res += [curranno]
    return res


def old_extract_annotations(infile, annotype, verbose, refname):
    pos = INSTART
    res = []
    curranno = None
    numanno = 0
    cache = ""
    for line in infile:
        dat = [x for x in re.split('\s+', line) if len(x) != 0]
        print(dat)
        try:
            if pos == INSTART:
                if len(dat) > 0 and dat[0] == "FEATURES":
                    pos = INFEATURE
                continue
            elif pos == INFEATURE:
                if curranno is not None and dat[0] != "ORIGIN":
                        if dat[0][0] != "/":
                            cache += ' ' + line.strip().split('"')[0]
                        dat = dat[0].split("=")
                        if dat[0] == "/gene":
                            curranno.gene = dat[1].split('"')[1]
                            cache = curranno.gene
                        elif dat[0] == "/protein_id":
                            curranno.protein_id = dat[1].split('"')[1]
                            cache = curranno.protein_id
                        elif dat[0] == "/product":
                            curranno.product = line.strip().split('"')[1]
                            cache = curranno.product
                        elif "UniProtKB" in dat[1]:
                            curranno.uniprot = dat[1].split(':')[1][:-1]
                            cache = curranno.uniprot
                        elif dat[0] == "/locus_tag":
                            curranno.locus_tag = dat[1].split('"')[1]
                            cache = curranno.locus_tag
                        else:
                            cache = ""
                elif dat[0] == "ORIGIN":
                    pos = INSEQ
                    if curranno is not None:
                        res += [curranno]
                    return res
                else:
                    if curranno is not None:
                        res += [curranno]
                        curranno = None
                        numanno += 1
                        if verbose:
                            if numanno % 100 == 0:
                                sys.stdout.write("Read " + str(numanno) + " annotations...\r")
                                sys.stdout.flush()
                    annoname = dat[0]
                    if annoname == annotype:
                        annopos = dat[1]
                        if annopos[0] == "c":
                            annopos = annopos.split("(")[1][:-1]
                        annopos = [int(x) for x in annopos.split("..")]
                        curranno = Anno(min(annopos), max(annopos), refname)
        except IndexError:
            raise
    if verbose:
        sys.stdout.write("Done reading " + str(numanno) + " annotations.\n")
    with open('debug.txt', 'w') as f:
        f.write(res)
    return res

def extract_sequence(infile, verbose):
    if verbose:
        sys.stdout.write("Reading sequence...\r")
        sys.stdout.flush()
    res = []
    numline = 0
    for line in infile:
        if line[0] == "/":
            seq = "".join(res)
            if verbose:
                sys.stdout.write("Done reading " + str(len(seq)) + " bases of sequence.\n")
                sys.stdout.flush()
            return "".join(seq)
        res += line.strip().split(" ")[1:]
        numline += 1
        if verbose and numline % 1000 == 0:
            sys.stdout.write("Read " + str(numline) + " lines of sequence...\r")
            sys.stdout.flush()

def write_sequence(outfile, seq, name, splitlen, verbose):
    if verbose:
        sys.stdout.write("Writing sequence...\r")
        sys.stdout.flush()
    f = open(outfile, "w")
    f.write(">" + name + "\n")
    pos = 0
    numline = 0
    while pos < len(seq):
        f.write(seq[pos:pos+splitlen])
        pos += splitlen
        f.write("\n")
        numline += 1
        if verbose and numline % 1000 == 0:
            sys.stdout.write("Wrote " + str(numline) + " lines of sequence...\r")
            sys.stdout.flush()
    if verbose:
        sys.stdout.write("Done writing " + str(len(seq)) + " bases of sequence in " + str(numline) + " lines.\n")
        sys.stdout.flush()

def write_annotations(outfile, annotations):
    f = open(outfile, "w")
    for anno in annotations:
        if anno.has_some_name():
            #f.write(anno.refname + "\t" + anno.get_some_name() + "\t" + str(anno.annostart) + "\t" + str(anno.annoend) + "\n")
            #f.write(str(anno.get_locus_tag()) + "\t" + anno.get_some_name() + "\t" + anno.get_product() + "\t" + anno.get_uniprot() + "\n")
            #f.write('{}\t{}\t{}\t{}\n'.format(anno.get_locus_tag(), anno.get_gene_name(), anno.get_protein_id(), anno.get_product(), anno.get_uniprot()))
            f.write('{}\t{}\t{}\t{}\n'.format(anno.get_gene_name(), anno.get_locus_tag(), anno.get_protein_id(), anno.get_product(), anno.get_uniprot()))
    f.close()

def write_fasta_get_dict(infile, fastafile):
    d = defaultdict(list)
    with open(infile, 'r') as f:
        name = extract_name(f, False)
        annotations = extract_annotations(f, 'CDS', False, name)
        seq = extract_sequence(f, False)
    write_sequence(fastafile, seq, name, 80, False)
    for anno in annotations:
        #d[anno.get_locus_tag()] += [anno.get_gene_name(), anno.get_protein_id(), anno.get_product(), anno.get_uniprot()]
        d[anno.get_gene_name()] += [anno.get_locus_tag(), anno.get_protein_id(), anno.get_product(), anno.get_uniprot()]
    return d

def write_fasta(infile, fastafile):
    from Bio import SeqIO

    with open(infile, 'r') as f, open(fastafile, 'w') as g:
        sequences = SeqIO.parse(f, "genbank")
        for record in sequences:
            #x = record.id
            #record.id = x.split('.')[0]
            if record.name is not None:
                record.id = record.name
            SeqIO.write(record, g, "fasta")
    #with open(infile, 'r') as f:
    #    name = extract_name(f, False)
    #    annotations = extract_annotations(f, 'CDS', False, name)
    #    seq = extract_sequence(f, False)
    #write_sequence(fastafile, seq, name, 80, False)

def get_dict(infile):
    d = defaultdict(list)
    with open(infile, 'r') as f:
        name = extract_name(f, False)
        annotations = extract_annotations(f, 'CDS', False, name)
    for anno in annotations:
        #d[anno.get_locus_tag()] += [anno.get_gene_name(), anno.get_protein_id(), anno.get_product(), anno.get_uniprot()]
        if anno.get_gene_name():
            d[anno.get_gene_name()] += [anno.get_gene_name(), anno.get_protein_id(), anno.get_product(), anno.get_uniprot()]
        else:
            d[anno.get_locus_tag()] += [anno.get_locus_tag(), anno.get_protein_id(), anno.get_product(), anno.get_uniprot()]
    return d

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("infile", help="Input file in Genbank format")
    parser.add_argument("annofile", help="Output file, will contain extracted annotations (for countAll.py)")
    parser.add_argument("fastafile", help="Output file, will contain extacted sequence in FASTA format")
    parser.add_argument("-a", "--annotationtype", help="Type of annotation to extract (code from FEATURES section of the GB file)", default="CDS")
    parser.add_argument("-v", "--verbose", help="Enable verbose output", action="store_true")
    parser.add_argument("-l", "--linelen", help="Length of sequence lines in FASTA file", default=80)

    args = parser.parse_args()

    f = open(args.infile, "r")
    name = extract_name(f, args.verbose)
    annotations = extract_annotations(f, args.annotationtype, args.verbose, name)
    write_annotations(args.annofile, annotations)
    seq = extract_sequence(f, args.verbose)
    f.close()
    write_sequence(args.fastafile, seq, name, args.linelen, args.verbose)
