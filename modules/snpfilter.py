#!/usr/bin/env python
# -*- coding: utf-8 -*-
#created by Stephan Fuchs (FG13, RKI), 2015
# A manuscript describing this program is in preparation. However, as long as it has been not published please contact Stephan Fuchs (fuchss@rki.de) for redistributing and/or modifying this program.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# copyright (c) 2016 Stephan Fuchs (fuchss@rki.de)

# Edited for use in the VariantCalling pipeline by Enrico Seiler (seilere@rki.de)

#config
version = '2.2.0'
logsep = '=====================================\n'
ambiguous = set(['M', 'R', 'W', 'S', 'Y', 'K', 'V', 'H', 'D', 'B', 'X', 'N'])


#dependencies
try:
	import argparse
except ImportError:
	#print('The "argparse" module is not available. Use Python 3.2 or better.')
	sys.exit(1)
	
import os
import sys
import time
import tempfile
import subprocess
from Bio import SeqIO

def parse_args(string=None):
    #processing command line arguments
    parser = argparse.ArgumentParser(prog="SNPFilter", description='Filtering SNPs from aligned sequences')
    parser.add_argument('rfile', metavar="REFERENCE_FILE", help="input FASTA formatted file containing reference sequence which was used as template for SNP calling", type=argparse.FileType('r'))
    parser.add_argument('afile', metavar="ALIGNMENT_FILE", help="input FASTA formatted file containing aligned SNP containing sequences", type=argparse.FileType('r'))
    parser.add_argument('out_csv', help="output csv", type=str)
    parser.add_argument('out_log', help="output log", type=str)
    parser.add_argument('out_fna', help="output fna", type=str)
    parser.add_argument('-d', '--exclusion-distance', metavar='INT', help="SNPs accumulating within this distance will be not considered. By default exclusion distance is set to zero.", type=int, default=0)
    parser.add_argument('-m', '--molecule', help="specifiy type of molecule as 'circular' or 'linear'. By default molecule type is set to 'circular'.", choices=['circular', 'linear'], default='circular')
    parser.add_argument('-a', help="include ambiguous sites. This is not recommended for phylogenetic analyses. Thus, sites containing ambiguous nucleotides (" + ", ".join(ambiguous) + ") in at least one of the sequences are not considered by default.", action="store_true")
    parser.add_argument('-g', help="include gaps. By default sites containing gaps (-) in at least one of the sequences are not considered.", action="store_true")
    #parser.add_argument('-o', '--outdir', metavar='DIR', help="use this output directory. By default a output directory is generated based on alignment file name and timestamp. If you use this option, a timestamp-based subdirectory is created within the submitted directory.", action="store")
    parser.add_argument('-f', help=" consider excluded SNPs in SNP table file. Particularly in combination with option -a or -g execution time and size of the resulting SNP table file can be dramatically increased. Don't say we didn't warn you!", action="store_true")
    parser.add_argument('--mask', metavar="MASKED_REGION_FILE", help="input file containing regions to exclude. The file has to contain two columns for region start (1st col) and region end (2nd col).", type=argparse.FileType('r'))
    parser.add_argument('--version', action='version', version='%(prog)s ' + version) 
    #print(string)
    if string is None:
        return parser.parse_args() 
    else:
        return parser.parse_args(string)
    ##print(args)


#functions
def createfileIndex(fname, extensions, sep = '_'):
	'''if suggested file name already exists on file system file_names are appended by a number. Returns the proposed file name without extension(s).'''
	err = 1
	index = 0
	add = ''
	while err == 1:
		err = 0
		for ext in extensions:
			if os.path.isfile(fname + add + '.' + ext):
				err = 1
		if err == 1:
			index += 1
			add = sep + str(index)
	return fname + add	 

   
def distancefilter(positions, exclusion_distance, molecule_type, refseq, seqs):
	'''filters SNPs from a stack of aligned sequences distance-based. Importantly, the algorithm is sequence-specific. Each submitted SNP position is only considered if nucleotide is different to the reference sequence at the respective position'''
	exclude_pos=[]	
	seqlen = len(refseq)
	
	#built sequence-specific position vectors of sequence-specific SNPs
	for j in range(len(seqs)):
		posvector = []
		for i in range(len(positions)):
			if seqs[j][positions[i]] != refseq[positions[i]]:
				posvector.append(positions[i])
	
		#distance filtering
		if len(posvector) > 1:
			if molecule_type == "circular" and posvector[0] + seqlen - posvector[-1] <= exclusion_distance:
				exclude_pos.extend([posvector[-1], posvector[0]])
			
			for i in range(len(posvector)-1):
				if posvector[i+1] - posvector[i] <= exclusion_distance:
					exclude_pos.extend([posvector[i], posvector[i+1]])

	return list(set(exclude_pos))
  
  
def formatColumns(rows, space=5):
	'''formats elements of list (rows) in columns based on tabulators. fields containing integers or floats are aligned to the right''' 
	
	#allowed signs in numbers
	allowed = ['+', '-', '.', ',']
	
	#split in columns
	rows = [x.split('\t') for x in rows] 
	#fill up rows to same number of columns (important for transposing)
	maxlen = max([len(x) for x in rows])
	[x.extend(['']*(maxlen-len(x))) for x in rows]
	
	align = []
	output = [''] * len(rows)
	for column in zip(*rows):
		#detect numbers to align right
		string = ''.join(column)
		for i in allowed:
			string = string.replace(i, '')
		if string.strip('+-.,').isdigit():
			align = 'r'
		else:
			align = 'l'
	
		maxlen = max([len(x) for x in column])	
		for i in range(len(column)):
			if align == 'r':
				output[i] += ' ' * (maxlen - len(column[i])) + column[i] + ' ' * space
			else:
				output[i] += column[i] + ' ' * (maxlen - len(column[i])) + ' ' * space	

	return '\n'.join(output) + '\n'

def main(args):
    #START

    #timer
    start_time = time.time()

    #filing
    args.rfile.close()
    args.afile.close()

    #work directory
    # if args.outdir:
            # args.outdir = args.outdir.strip()
            # if args.outdir[-1] != '/':
                    # args.outdir += '/'  
            
    ##print("preparing result directory:  " + workdir + "/")

    #output files
    logfile = args.out_log
    #print("preparing log file:          " + logfile.split("/")[-1])
    loghandle = open(logfile, 'w')

    snpfile = args.out_fna
    #print("preparing output file:       " + snpfile.split("/")[-1])
    snphandle = open(snpfile, 'w')

    tablefile = args.out_csv
    #print("preparing SNP table file:    " + tablefile.split("/")[-1])
    tablehandle = open(tablefile, 'w')	
    tableinfo = {}

    #process masked region
    masked_regions = []
    masked = 0
    if args.mask:
            for line in args.mask:
                    line = line.strip()
                    if line != "":
                            fields = line.split('\t')
                            if len(fields) != 2:
                                    #print("ERROR: mask file does not meet the format criteria")
                                    loghandle.write('\nERROR\n')
                                    loghandle.write(logsep)   
                                    loghandle.write('mask file does not meet the format criteria\naborted')		
                                    sys.exit(1)			
                            if (fields[0] + fields[1]).isdigit() != True:
                                    #print("ERROR: coordinates in mask file have to be integer")
                                    loghandle.write('\nERROR\n')
                                    loghandle.write(logsep)   
                                    loghandle.write('coordinates in mask file have to be integer\naborted')		
                                    sys.exit(1)
                            if int(fields[0]) > int(fields[1]):			
                                    #print("ERROR: start coordinates has be lower than end coordinates for all regions in mask file")
                                    loghandle.write('\nERROR\n')
                                    loghandle.write(logsep)   
                                    loghandle.write('start coordinates has be lower than end coordinates for all regions in mask file\naborted')		
                                    sys.exit(1)			
                            masked += 1				
                            masked_regions.extend(range(int(fields[0])-1, int(fields[1])))

    masked_regions = set(masked_regions)

    #logging used parameter
    #print ('logging used parameters ...')
    loghandle.write('\nSNP FILTER\n')
    loghandle.write(logsep)   
    logging = []
    logging.append('version:\t' + version)
    logging.append('timestamp:\t' + time.strftime("%Y-%m-%d %H:%M:%S"))
    logging.append('reference file:\t' + args.rfile.name)
    logging.append('alignment file:\t' + args.afile.name)
    if args.mask:
            logging.append('masked regions file:\t' + args.mask.name + ' (' + str(masked) + ' regions)')
    else:
            logging.append('masked regions file:\t-')
    logging.append('exclusion distance:\t' + str(args.exclusion_distance))
    logging.append('molecule type:\t' + str(args.molecule))

    if args.a:
            logging.append('ambiguous sites:\tconsidered')
    else:  
            logging.append('ambiguous sites:\tnot considered')
    if args.g:
            logging.append('sites with gaps:\tconsidered')
    else:  
            logging.append('sites with gaps:\tnot considered')	
    if args.f:
            logging.append('excluded SNPs in SNP table:\tyes')
    else:  
            logging.append('excluded SNPs in SNP table:\tno')	

    loghandle.write(formatColumns(logging))
       

    #file inspection
    #print ('inspecting input files ...')
    logging = []
    loghandle.write('\ninput file inspection\n')
    loghandle.write(logsep)	

    n = 0

    #reference file
    for seq_record in SeqIO.parse(args.rfile.name, "fasta"):
            if n > 0 :
                    #print("ERROR: reference file contains multiple entries")
                    loghandle.write('\nERROR\n')
                    loghandle.write(logsep)   
                    loghandle.write('ERROR: reference file contains multiple entries\naborted')
                    sys.exit(1)
                    
            n += 1
            reflength = len(seq_record)
            refseq = str(seq_record.seq).upper()
            refid = str(seq_record.description).replace('\t', ' ')	
            logging.append(str(n) + ".\t" + refid + "\t" + str(reflength) + 'nt\treference')

    if n < 1:
            #print("ERROR: reference file contains no FASTA formatted entry")
            loghandle.write('\nERROR\n')
            loghandle.write(logsep)   
            loghandle.write('ERROR: reference file contains no FASTA formatted entry\naborted')
            sys.exit(1)	

    #alignment file
    lenerr = 0
    ids = []
    seqs = []
    for seq_record in SeqIO.parse(args.afile.name, "fasta"):
            seqid = str(seq_record.description).replace('\t', ' ') 
            if seqid != refid or str(refseq) != str(seq_record.seq).upper():
                    n += 1
            if len(seq_record) == reflength:
                    lentest="ok!"
            elif len(seq_record) > reflength:		
                    lentest = "too long!"	  
                    lenerr = 1
            else:
                    lentest = "too short!"	  
                    lenerr = 1
            seqs.append(str(seq_record.seq).upper())
            ids.append(seqid)
            logging.append(str(n) + ".\t" + seqid + "\t" + str(len(seq_record)) + 'nt\t' + lentest)

    loghandle.write(formatColumns(logging))	

    if n < 2:
            #print("ERROR: alignment file contains no FASTA formatted entry")
            loghandle.write('\nERROR\n')
            loghandle.write(logsep)   
            loghandle.write('ERROR: alignment file contains no FASTA formatted entry\naborted')
            sys.exit(1) 
            
    #length test
    if lenerr == 1:
            #print("ERROR: sequences differ in length (see log file for details)")
            loghandle.write('\nERROR\n')
            loghandle.write(logsep)   
            loghandle.write('sequences differ in length\naborted')
            sys.exit(1)
            
    #length mask test
    if len(masked_regions) and max(masked_regions) > reflength:
            #print("ERROR: there are masked regions which are outside the sequences.")
            loghandle.write('\nERROR\n')
            loghandle.write(logsep)   
            loghandle.write('ERROR: there are masked regions which are outside the sequences\naborted')
            sys.exit(1) 	
            
    #SNP filter
    #print ('filtering SNPs ...')
    logging = []
    loghandle.write('\nsnp statistics\n')
    loghandle.write(logsep) 

    snps = []
    seqs.insert(0, refseq) #add reference sequence to seqs for positiontupel, necessary for SNP filtering
    positiontupel=list(zip(*seqs)) #includes reference
    del seqs[0]  #remove reference sequence from seqs 

    snps = {}
    stat = {}
    stat['masked'] = 0
    stat['identical'] = 0
    stat['ambiguous'] = 0
    stat['variant'] = 0
    stat['gaps'] = 0
    pos = -1
    for i in range(len(positiontupel)):
            pos += 1
            jump = 0
            bases = list(set(positiontupel[i]))
            #identical
            if len(bases) == 1:
                    stat['identical'] += 1
                    continue
            #ambiguous
            if len(ambiguous.intersection(bases)) > 0:
                    stat['ambiguous'] += 1
                    stat['variant'] += 1
                    if args.a or args.f:
                            tableinfo[pos] = ['a']	
                    if args.a == False:
                            jump = 1 # do not use continue since gap and ambiguous can be both present
            #masked
            if pos in masked_regions:
                    stat['masked'] += 1
                    jump = 1
                    if args.f:
                            if pos in tableinfo:
                                    tableinfo[pos] += ['m']
                            else:
                                    tableinfo[pos] = ['m']		
            
            #gap
            if '-' in bases :
                    stat['gaps'] += 1
                    stat['variant'] += 1
                    if args.g or args.f:
                            if pos in tableinfo:
                                    tableinfo[pos] += ['g']
                            else:
                                    tableinfo[pos] = ['g']
                    if args.g == False:
                            jump = 1  
            
            if jump == 0:
                    stat['variant'] += 1
                    snps[pos] = positiontupel[i]
                    if pos in tableinfo:
                            tableinfo[pos] += ['v']
                    else:
                            tableinfo[pos] = ['v']

    logging.append('non-variant positions:\t' + str(stat['identical']) + '\t' + '{0:5.2f}'.format(round(float(stat['identical'])/float(reflength)*100,2)) + '% of the genome')

    logging.append('total SNP positions:\t' + str(stat['variant']) + '\t' + '{0:5.2f}'.format(round(float(stat['variant'])/float(reflength)*100,2)) + '% of the genome')

    if stat['variant'] == 0:
            logging.append('masked SNP positions:\t' + str(stat['masked']))
            logging.append('SNP positions including ambiguous sites:\t'+ str(stat['ambiguous'])) 
            logging.append('SNP positions including gaps:\t' + str(stat['gaps']))
    else:
            logging.append('masked SNP positions:\t' + str(stat['masked']) + '\t' + '{0:5.2f}'.format(round(float(stat['masked'])/float(stat['variant'])*100,2)) + '% of all SNP positions')
            logging.append('SNP positions including ambiguous sites:\t' + str(stat['ambiguous']) + '\t' + str(round(float(stat['ambiguous'])/float(stat['variant'])*100,2)) + '% of all SNP positions') 
            logging.append('SNP positions including gaps:\t' + str(stat['gaps']) + '\t' + '{0:5.2f}'.format(round(float(stat['gaps'])/float(stat['variant'])*100,2)) + '% of all SNP positions') 

    #distance exclusion
    if args.exclusion_distance > 0:
            #print('performing distance exclusion ...')
            if stat['variant'] == 0:
                    logging.append('SNP positions excluded by distance:\t0')
            else:
                    positions = snps.keys()
                    positions.sort(key=int)
                    exclude_pos = distancefilter(positions, args.exclusion_distance, args.molecule, refseq, seqs)
                    
                    stat['distance_excluded'] = len(exclude_pos)
                    logging.append('SNP positions excluded by distance:\t' + str(stat['distance_excluded']) + '\t' + '{0:5.2f}'.format(round(float(stat['distance_excluded'])/float(stat['variant'])*100,2)) + '% of all SNP positions')	 
                    
                    for i in exclude_pos:
                            del snps[i]	
                            if args.f:
                                    tableinfo[i].extend('d')
                            else:
                                    del tableinfo[i]

    logging.append(' ')
    if stat['variant'] > 0:
            logging.append('saved SNP positions\t' + str(len(snps)) + '\t' + '{0:5.2f}'.format(round(float(len(snps))/float(stat['variant'])*100,2)) + '% of all SNP positions')  

    loghandle.write(formatColumns(logging))

    #fna file output
    #print ('writing output file ...')

    positions = list(snps.keys())
    positions.sort(key=int)

    seqtupel = []
    for pos in positions:
            seqtupel.append(snps[pos])
            
    n = 0
    ids.insert(0, refid)
    for seq in zip(*seqtupel):
            snphandle.write('>' + ids[n] + '\n')
            snphandle.write(''.join(seq) + '\n')
            n += 1
              
    snphandle.close()


    #csv file output
    #print ('writing snp table file ...')
    if args.f:
            tablehandle.write('### NOTE: - means false / + means true / ++ means true and causing filtering ###\n')

    tablehandle.write('coordinate,')
    if args.a or args.f:
            tablehandle.write('ambiguity,')
    if args.g or args.f:
            tablehandle.write('gap,')
    if args.f:
            tablehandle.write('masked,distance_exclusion,filtered,')
    tablehandle.write(','.join(ids).replace(" ", "_"))
                      
    positions = list(tableinfo.keys())
    positions.sort(key=int)
    for i in positions:
            line = [str(i+1)]
            snpfilter = 0
                    
            if args.a or args.f:	
                    if 'a' in tableinfo[i]:
                            if args.a:
                                    line.append('+')
                            else:
                                    line.append('++')		  
                                    snpfilter=1
                    else:
                            line.append('-')
            
            if args.g or args.f: 
                    if 'g' in tableinfo[i]:
                            if args.g:
                                    line.append('+')
                            else:
                                    line.append('++')
                                    snpfilter=1
                    else:
                            line.append('-')
            
            if args.f: 
                    if 'm' in tableinfo[i]:
                            line.append('++')
                            snpfilter=1	
                    else:
                            line.append('-')		
                    
                    if 'd' in tableinfo[i]:
                            line.append('++')
                            snpfilter=1
                    else:
                            line.append('-')
                    

            if snpfilter == 1:
                    line.append('+')
            else:
                    line.append('-')			  
            
            line.extend(positiontupel[i])	
            tablehandle.write("\n" + ",".join(line))

    tablehandle.close()

                    

    #finishing
    logging = []
    loghandle.write('\noutput files\n')
    loghandle.write(logsep) 
    logging.append('snp file:\t' + snpfile)
    logging.append('log file:\t' + logfile)
    logging.append('snp table file:\t' + tablefile)

    loghandle.write(formatColumns(logging))	

    loghandle.write("\n--- execution time: %s seconds ---" % (time.time() - start_time)) 

    loghandle.close()
    #print("show log file:\n\n")

    loghandle=open(logfile, 'r')
    #for line in loghandle:
            #print("\t" + line.replace("\n", ""))
    loghandle.close()
    #print("\n\n")  

if __name__=='__main__':
    main(parse_args())

def run(rfile, afile, out_csv, out_log, out_fna, options):
    #print(rfile)
    #print(afile)
    #print(out_csv)
    #print(out_fna)
    #print(options)
    if len(options) > 0:
        option_list = options.split(' ')
        string = [rfile, afile, out_csv, out_log, out_fna] + option_list
    else:
        string = [rfile, afile, out_csv, out_log, out_fna]
    main(parse_args(string=string))
