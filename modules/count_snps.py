#! /usr/bin/python3

import pandas as pd
import argparse
import re
import math

def get_sample_dict(table):
    sample_pattern = re.compile('^#\s*(S\d*)\s*:\s*(.*)')
    samplenames = dict()
    with open(table, 'r') as f:
        for line in f:
            try:
                [identifier, sample] = re.match(sample_pattern, line).groups()
                samplenames[identifier] = sample
            except:
                continue
    return samplenames

def run(complete):

    parser = argparse.ArgumentParser()
    parser.add_argument('complete')
    args = parser.parse_args([complete])

    df = pd.read_table(args.complete, comment='#')
    ncol = len(df.columns)
    nsamples = (ncol - 15) // 5
    samplenames = get_sample_dict(args.complete)

    snp_called = dict()
    snp_silent = dict()
    snp_aachange = dict()
    snp_noorf = dict()
    snp_uncalled = dict()
    snp_nocov = dict()
    for i in range(nsamples):
        snp_called[samplenames['S{}'.format(i+1)]] = 0
        snp_silent[samplenames['S{}'.format(i+1)]] = 0
        snp_aachange[samplenames['S{}'.format(i+1)]] = 0
        snp_noorf[samplenames['S{}'.format(i+1)]] = 0
        snp_uncalled[samplenames['S{}'.format(i+1)]] = 0
        snp_nocov[samplenames['S{}'.format(i+1)]] = 0

    for name, snp in df.iterrows():
        for i in range(nsamples):
            snp_gt = snp['S{}.GT'.format(i+1)]
            snp_gt = snp_gt.split('/')[1] if '/' in snp_gt else snp_gt
            # snp_nocov
            if math.isnan(snp['S{}.COV'.format(i+1)]):
                snp_nocov[samplenames['S{}'.format(i+1)]] += 1
            # snp_called
            elif snp_gt == snp['ALT']:
                snp_called[samplenames['S{}'.format(i+1)]] += 1
                # snp_aachange
                if snp['AAR'] != snp['AAA']:
                    snp_aachange[samplenames['S{}'.format(i+1)]] += 1
                # snp_silent
                else:
                    snp_silent[samplenames['S{}'.format(i+1)]] += 1
                # snp_noorf
                if snp['TYPE'] in ['upstream_gene_variant', 'downstream_gene_variant', 'snpeff_missing']:
                    snp_noorf[samplenames['S{}'.format(i+1)]] += 1
            # snp_uncalled
            else:
                snp_uncalled[samplenames['S{}'.format(i+1)]] += 1

    return [snp_called, snp_silent, snp_aachange, snp_noorf, snp_uncalled, snp_nocov, list(samplenames.values())]

if __name__ == '__main__':
    import sys
    run(sys.argv[1])





