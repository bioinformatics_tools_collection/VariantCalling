__version__ = 'dev_1'
import datetime
import jinja2
import platform
import pwd
import socket
if sys.version_info < (3, 2):
    import subprocess32 as sp
else:
    import subprocess as sp
import os
import re
import extract_genbank
import format_table
import count_snps
import msa
import snpfilter
from collections import defaultdict
JAVA = config.get("JAVA")
ANNOTATE_ALL = config.get("ANNOTATE_ALL")
DECIMAL = config.get("DECIMAL")
OUTPUT = config.get("OUTPUT")
LOG = config.get("LOG")
GB_FILE = config.get("GB_FILE")
MANIP_GB_FILE = ''.join([os.path.join(OUTPUT, os.path.basename(GB_FILE)), '.manip'])
SNPEFF_FOLDER = config.get("SNPEFF_FOLDER")
GATK_FOLDER = config.get("GATK_FOLDER")
PICARD_FOLDER = config.get("PICARD_FOLDER")
ELPREP_FOLDER = config.get("ELPREP_FOLDER")
PLATFORM = config.get("PLATFORM")
if ELPREP_FOLDER == 'NA':
    ELPREP_FOLDER = None
SAMTOOLS_FOLDER = config.get("SAMTOOLS_FOLDER")
PLOIDY = config.get("PLOIDY")
MSA = config.get("MSA")
AMBIGUITY_CUTOFF = config.get("AMBIGUITY_CUTOFF")
MAJORITY = config.get("MAJORITY")
SNPFILTER = config.get("SNPFILTER")
SNPFILTER_CONFIG = config.get("SNPFILTER_CONFIG")
if ELPREP_FOLDER == 'None' or ELPREP_FOLDER is None:
    ELPREP_FOLDER = False
SAM_FILE = config.get("SAM_FILE")
if SAM_FILE.lower().endswith('.bam'):
    BAM_FILE = SAM_FILE
    bam = True
    C_BAM_FILE = ''.join([os.path.join(OUTPUT, os.path.basename(BAM_FILE[:-4])), '_clean.bam'])
    #F_BAM_FILE = ''.join([C_BAM_FILE[:-4], '_fixed.bam'])
    RG_BAM_FILE = ''.join([C_BAM_FILE[:-4], '_RG.bam'])
    MD_BAM_FILE = ''.join([RG_BAM_FILE[:-4], '_MD.bam'])
    FASTA_FILE = ''.join([os.path.join(OUTPUT, os.path.basename(GB_FILE[:-3])), '.fasta'])
    FASTA_DICT = ''.join([os.path.join(OUTPUT, os.path.basename(FASTA_FILE[:-6])), '.dict'])
    VCF_FILE = ''.join([os.path.join(OUTPUT, os.path.basename(BAM_FILE[:-4])), '.vcf'])
    VCF_TABLE = ''.join([os.path.join(OUTPUT, os.path.basename(BAM_FILE[:-4])), '.table'])
    TABLE_COMPLETE = ''.join([os.path.join(OUTPUT, os.path.basename(BAM_FILE[:-4])), '.complete'])
    VCF_ANNOTATED_FILE = ''.join([VCF_FILE[:-4], '_annotated.vcf'])
    SEQUENCE_ALIGNMENT = ''.join([VCF_FILE[:-4], '_msa', '.fasta'])
    MSA_REFERENCE = ''.join([VCF_FILE[:-4], '_msa_reference', '.fasta'])
    REPORT = ''.join([VCF_FILE[:-4], '_report', '.tex'])
    CSV_FILTERED = ''.join([VCF_FILE[:-4], '_filtered', '.csv'])
else:
    bam = False
    BAM_FILE = ''.join([os.path.join(OUTPUT, os.path.basename(SAM_FILE[:-4])), '.bam'])
    C_BAM_FILE = ''.join([BAM_FILE[:-4], '_clean.bam'])
    #F_BAM_FILE = ''.join([C_BAM_FILE[:-4], '_fixed.bam'])
    RG_BAM_FILE = ''.join([C_BAM_FILE[:-4], '_RG.bam'])
    MD_BAM_FILE = ''.join([RG_BAM_FILE[:-4], '_MD.bam'])
    FASTA_FILE = ''.join([os.path.join(OUTPUT, os.path.basename(GB_FILE[:-3])), '.fasta'])
    FASTA_DICT = ''.join([os.path.join(OUTPUT, os.path.basename(FASTA_FILE[:-6])), '.dict'])
    VCF_FILE = ''.join([BAM_FILE[:-4], '.vcf'])
    VCF_TABLE = ''.join([BAM_FILE[:-4], '.table'])
    TABLE_COMPLETE = ''.join([BAM_FILE[:-4], '.complete'])
    VCF_ANNOTATED_FILE = ''.join([VCF_FILE[:-4], '_annotated.vcf'])
    SEQUENCE_ALIGNMENT = ''.join([VCF_FILE[:-4], '_msa', '.fasta'])
    MSA_REFERENCE = ''.join([VCF_FILE[:-4], '_msa_reference', '.fasta'])
    REPORT = ''.join([VCF_FILE[:-4], '_report', '.tex'])
    CSV_FILTERED = ''.join([VCF_FILE[:-4], '_filtered', '.csv'])

def func():
    if SNPFILTER:
        return CSV_FILTERED
    elif MSA:
        return SEQUENCE_ALIGNMENT
    else:
        return TABLE_COMPLETE

rule all:
    input:
        #REPORT if not MSA else SEQUENCE_ALIGNMENT
        REPORT,
        REPORT[:-3] + 'pdf',
        func()

rule extract_fasta:
    input:
        GB_FILE
    output:
        temp(FASTA_FILE)
    run:
        if GB_FILE.endswith('.fa') or GB_FILE.endswith('.fasta'):
            shell('cp {} {}'.format(input, output))
        else:
            extract_genbank.write_fasta(GB_FILE, FASTA_FILE)

if ELPREP_FOLDER:
    rule process_alignment:
        input:
            SAM_FILE
        output:
            MD_BAM_FILE
        threads:
            64
        run:
            rg_string = ''
            # readgroups
            global RG_SAM_FILE
            samtools = os.path.join(SAMTOOLS_FOLDER, 'samtools')
            try:
                shell("(( $({samtools} view -h {SAM_FILE} | head -n 100 | grep '^@RG' -c) >= 1))")
                readgroups_present = True
            except:
                readgroups_present = False
            if not readgroups_present:
                try: # Raw
                    rg_sm = re.match("(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", os.path.basename(SAM_FILE)).group(1)
                except AttributeError:
                    try: # QCumber
                        rg_sm = re.match("(.*?)(\.[1,2][U,P]){0,1}$.*", os.path.basename(SAM_FILE)).group(1)
                    except AttributeError:
                        rg_sm = os.path.basename(self.query[0]).split('.')[0]
                rg_pl = PLATFORM
                rg_id = rg_sm
                rg_pu = rg_id
                rg_lb = 'lib_' + rg_sm
                rg_string = '"ID:{} LB:{} PL:{} PU:{} SM:{}"'.format(rg_id, rg_lb, rg_pl, rg_pu, rg_sm)
            # elPrep
            if rg_string != '':
                shell('{} {} {} --filter-unmapped-reads --replace-read-group {} --mark-duplicates remove --clean-sam --sorting-order coordinate --nr-of-threads {} >>{} 2>&1'.format(os.path.join(ELPREP_FOLDER, 'elprep'), SAM_FILE, MD_BAM_FILE, rg_string, threads, LOG))
            else:
                shell('{} {} {} --filter-unmapped-reads --mark-duplicates remove --clean-sam --sorting-order coordinate --nr-of-threads {} >>{} 2>&1'.format(os.path.join(ELPREP_FOLDER, 'elprep'), SAM_FILE, MD_BAM_FILE, threads, LOG))
            shell('{samtools} index {} >>{} 2>&1'.format(MD_BAM_FILE, LOG))

else:
    rule convert_to_bam:
        input:
            SAM_FILE
        output:
            temp(BAM_FILE)
        run:
            samtools = os.path.join(SAMTOOLS_FOLDER, 'samtools')
            shell('{samtools} view -hb {input} -o {output}')

    rule clean_bam:
        input:
            BAM_FILE
        output:
            temp(C_BAM_FILE)
        shell:
            '{} -jar {} CleanSam I={} O={} >> {} 2>&1'.format(JAVA, os.path.join(PICARD_FOLDER, 'picard.jar'), SAM_FILE, C_BAM_FILE, LOG)
    """
    rule fix_bam:
        input:
            C_BAM_FILE
        output:
            temp(F_BAM_FILE)
        shell:
            '{} -jar {} FixMateInformation I={} O={} SO=coordinate >> {} 2>&1'.format(JAVA, os.path.join(PICARD_FOLDER, 'picard.jar'), C_BAM_FILE, F_BAM_FILE, LOG)
    """
    rule add_readgroups:
        input:
            SAM_FILE,
            C_BAM_FILE
        output:
            temp(RG_BAM_FILE)
        run:
            global SAM_FILE
            samtools = os.path.join(SAMTOOLS_FOLDER, 'samtools')
            try:
                shell("(( $({samtools} view -h {SAM_FILE} | head -n 100 | grep '^@RG' -c) >= 1))")
                readgroups_present = True
            except:
                readgroups_present = False
            if not readgroups_present:
                try: # Raw
                    rg_sm = re.match("(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", os.path.basename(SAM_FILE)).group(1)
                except AttributeError:
                    try: # QCumber
                        rg_sm = re.match("(.*?)(\.[1,2][U,P]){0,1}$.*", os.path.basename(SAM_FILE)).group(1)
                    except AttributeError:
                        rg_sm = os.path.basename(self.query[0]).split('.')[0]
                rg_pl = PLATFORM
                rg_id = rg_sm
                rg_pu = rg_id
                rg_lb = 'lib_' + rg_sm
                shell('{} -jar {} AddOrReplaceReadGroups I={} O={} RGID={} RGLB={} RGPL={} RGPU={} RGSM={} SORT_ORDER=coordinate >>{} 2>&1'.format(JAVA, os.path.join(PICARD_FOLDER, 'picard.jar'), C_BAM_FILE, RG_BAM_FILE, rg_id, rg_lb, rg_pl, rg_pu, rg_sm, LOG))
            else:
                shell('mv {} {}'.format(C_BAM_FILE, RG_BAM_FILE))

    rule mark_duplicates:
        input:
            RG_BAM_FILE
        output:
            MD_BAM_FILE
        run:
            shell('{} -jar {} MarkDuplicates INPUT={} OUTPUT={} METRICS_FILE=metrics.txt REMOVE_DUPLICATES=true CREATE_INDEX=true >>{} 2>&1'.format(JAVA, os.path.join(PICARD_FOLDER, 'picard.jar'), RG_BAM_FILE, MD_BAM_FILE, LOG))
            shell('rm metrics.txt')

rule prepare_fasta:
    input:
        FASTA_FILE
    output:
        temp(FASTA_DICT)
    run:
        pattern = re.compile('>gi\|\d*\|[^|]*\|([^|]*)\| (.*)')
        samtools = os.path.join(SAMTOOLS_FOLDER, 'samtools')
        with open(FASTA_FILE, 'r+') as f:
            d = f.readlines()
            f.seek(0)
            fl = d[0]
            del(d[0])
            if '>gi' in fl:
                f.write('>{}\n'.format(' '.join(re.match(pattern, fl).groups())))
                for l in d:
                    f.write(l)
                f.write('\n')
                f.truncate()
        shell('{samtools} faidx {input}')
        try:
            shell('rm {} 1>/dev/null 2>&1'.format(FASTA_DICT)) # Picard will throw error when dict already exists. Workaround for use with --forceall
        except:
            pass
        shell('{} -jar {} CreateSequenceDictionary R={} O={} >> {} 2>&1'.format(JAVA, os.path.join(PICARD_FOLDER, 'picard.jar'), FASTA_FILE, FASTA_DICT, LOG))

rule call_variants:
    input:
        FASTA_FILE,
        FASTA_DICT,
        MD_BAM_FILE
    output:
        temp(VCF_FILE)
    threads:
        64
    run:
        shell('{} -Xmx256g -jar {} -T HaplotypeCaller -R {} -I {} -o {} -nct {}  --sample_ploidy {} >>{} 2>&1'.format(JAVA, os.path.join(GATK_FOLDER, 'GenomeAnalysisTK.jar'), FASTA_FILE, MD_BAM_FILE, VCF_FILE, threads, PLOIDY, LOG))
        try:
            shell('rm {} 1>/dev/null 2>&1'.format(MD_BAM_FILE[:-3] + 'bai'))
        except:
            try:
                shell('rm {} 1>/dev/null 2>&1'.format(MD_BAM_FILE + '.bai'))
            except:
                pass

"""Throw out the chromosome= line if it exists."""
rule manipulate_genbank:
    input:
        GB_FILE
    output:
        temp(MANIP_GB_FILE)
    run:
        with open(GB_FILE, 'r') as r, open(MANIP_GB_FILE, 'w') as w:
            for line in r:
                if not "chromosome=" in line:
                    w.write(line)
            w.write('\n')
            w.truncate()

rule annotate_variants:
    input:
        SNPEFF_FOLDER,
        MANIP_GB_FILE,
        VCF_FILE
    output:
        VCF_ANNOTATED_FILE
    run:
        if GB_FILE.endswith('.fa') or GB_FILE.endswith('.fasta'):
            shell('cp {VCF_FILE} {VCF_ANNOTATED_FILE}')
        else:
            NAME = None
            ACCESSION = None
            # create snpEff DB
            with open(GB_FILE, 'r') as f:
                for line in f:
                    if line.startswith('LOCUS'):
                        LOCUS = re.match('LOCUS *([^ ]*) .*', line).group(1)
                    if line.startswith('SOURCE'):
                        NAME = re.match('SOURCE *([a-zA-Z0-9 \.]*?)', line).group(1).replace(' ', '_').replace('.', '_'). replace('__', '_')
                        print(NAME)
                        if NAME == '':
                            NAME = LOCUS
                            if NAME == '':
                                NAME = 'PLACEHOLDER'
                        break
                    elif line.startswith('ACCESSION'):
                        ACCESSION = re.match('ACCESSION *([a-zA-Z0-9_.\- ]*?)\n', line).group(1)
            if NAME is None:
                NAME = LOCUS
            shell('mkdir -p {}'.format(os.path.join(OUTPUT, 'data/{NAME}/')))
            shell('cp {} {}'.format(MANIP_GB_FILE, os.path.join(OUTPUT, 'data/{NAME}/genes.gbk')))
            try:
                with open(os.path.join(OUTPUT, 'snpEff.config'), 'r') as f:
                    d = "".join(f.readlines())
                    if not '{}.genome'.format(NAME) in d:
                        shell('echo \'{}.genome : \n\t{}.chromosomes : {}\' >> {}'.format(NAME, NAME, ACCESSION, os.path.join(OUTPUT, 'snpEff.config')))
            except IOError:
                shell('echo \'{}.genome : \n\t{}.chromosomes : {}\' >> {}'.format(NAME, NAME, ACCESSION, os.path.join(OUTPUT, 'snpEff.config')))
            try:
                shell('{} -jar {} build -v -c {} -genbank {} >> {} 2>&1'.format(JAVA, os.path.join(SNPEFF_FOLDER, 'snpEff.jar'), os.path.join(OUTPUT, 'snpEff.config'), NAME, LOG))
            except:
                print('Building snpEff failed')
            # run snpEff
            try:
                if not ANNOTATE_ALL:
                    shell('{} -jar {} ann -v -c {} -no-upstream -no-downstream -no-intron -no-intergenic -noLog -stats {} {} {} > {} 2>>{}'.format(JAVA, os.path.join(SNPEFF_FOLDER, 'snpEff.jar'), os.path.join(OUTPUT, 'snpEff.config'), os.path.join(OUTPUT, 'snpEff_summary.html'),  NAME, VCF_FILE, VCF_ANNOTATED_FILE, LOG))
                else:
                    shell('{} -jar {} ann -v -c {} -noLog -stats {} {} {} > {} 2>>{}'.format(JAVA, os.path.join(SNPEFF_FOLDER, 'snpEff.jar'), os.path.join(OUTPUT, 'snpEff.config'), os.path.join(OUTPUT, '{}_snpEff_summary.html'.format(os.path.splitext(os.path.basename(VCF_FILE))[0])), NAME, VCF_FILE, VCF_ANNOTATED_FILE, LOG))
            except:
                print('Running snpEff failed')
                shell('cp {VCF_FILE} {VCF_ANNOTATED_FILE}')
            # only want annotated variants
            #with open(VCF_ANNOTATED_FILE, 'r+') as f:
            #    d = f.readlines()
            #    f.seek(0)
            #    for l in d:
            #        if 'ANN=' in l or l.startswith('#'):
            #            f.write(l)
            #    f.truncate()
        try:
            shell('rm {}'.format(VCF_FILE + '.idx'))
        except:
            pass

        buff = ''
        with open(VCF_ANNOTATED_FILE, 'r+') as f:
            for line in f:
                if line.startswith('#'):
                    buff += line
                else:
                    frags = line.split('\t')
                    if not 'ANN=' in frags[7]:
                        buff += ('{}\t{}\t{}'.format('\t'.join(frags[:7]), frags[7]+';ANN=|snpeff_missing||||||||||||||', '\t'.join(frags[8:])))
                    else:
                        buff += line
            f.seek(0)
            f.write(buff)
        try:
            os.remove(os.path.join(OUTPUT, 'snpEff.config'))
        except:
            pass
        try:
            shell('rm -fdr {}'.format(os.path.join(OUTPUT, 'data/')))
        except:
            pass

rule vcf_table:
    input:
        FASTA_FILE,
        FASTA_DICT,
        VCF_ANNOTATED_FILE
    output:
        temp(VCF_TABLE)
    run:
        shell('{} -jar {} -R {} -T VariantsToTable -V {} -F CHROM -F POS -F REF -F ALT -F QUAL -F ANN -GF GT -GF GQ -GF DP -GF AD -o {} >> {} 2>&1'.format(JAVA, os.path.join(GATK_FOLDER, 'GenomeAnalysisTK.jar'), FASTA_FILE, VCF_ANNOTATED_FILE, VCF_TABLE, LOG))
        shell('rm {}'.format(VCF_ANNOTATED_FILE + '.idx'))
        shell('rm {}'.format(FASTA_FILE + '.fai'))

rule format_table:
    input:
        VCF_TABLE,
        GB_FILE
    output:
        TABLE_COMPLETE
    run:
        if GB_FILE.endswith('.fa') or GB_FILE.endswith('.fasta'):
            ANNO_DICT = None
        else:
            ANNO_DICT = extract_genbank.get_dict(GB_FILE)
        format_table.format_table(VCF_TABLE, TABLE_COMPLETE, ANNO_DICT, DECIMAL)

if MSA:
    rule consensus_sequence_alignment:
        input:
            TABLE_COMPLETE,
            MD_BAM_FILE,
            GB_FILE
        output:
            SEQUENCE_ALIGNMENT,
            temp(MSA_REFERENCE)
        run:
            if MAJORITY:
                msa.run(TABLE_COMPLETE, GB_FILE, MD_BAM_FILE, SEQUENCE_ALIGNMENT, AMBIGUITY_CUTOFF, '--majority')
            else:
                msa.run(TABLE_COMPLETE, GB_FILE, MD_BAM_FILE, SEQUENCE_ALIGNMENT, AMBIGUITY_CUTOFF, 'false')

if SNPFILTER:
    rule filter_snps:
        input:
            MSA_REFERENCE,
            SEQUENCE_ALIGNMENT
        output:
            CSV_FILTERED
        params:
            logfile = CSV_FILTERED[:-4] + '.log',
            snpfile = CSV_FILTERED[:-4] + '.fna'
        run:
            snpfilter.run(MSA_REFERENCE, SEQUENCE_ALIGNMENT, CSV_FILTERED, params.logfile, params.snpfile, SNPFILTER_CONFIG)

rule report:
    input:
        TABLE_COMPLETE
    output:
        REPORT,
        REPORT[:-3] + 'pdf'
    run:
        context = dict()
        context['name'] = 'VariantCalling Pipeline'
        context['version'] = __version__
        context['reference'] = os.path.realpath(GB_FILE)
        context['alignment'] = os.path.realpath(SAM_FILE)
        context['dataset'] = SAM_FILE
        context['output'] = OUTPUT
        context['python_version'] = sys.version.split(' ')[0]
        context['user'] = pwd.getpwuid(os.getuid()).pw_name
        context['server'] = socket.getfqdn()
        context['system'] = platform.system()
        context['machine'] = platform.architecture()[0]
        context['call'] = ' '.join(sys.argv)
        if context['system'] == 'Linux':
            context['linux_dist'] = platform.linux_distribution()[0]      # Ubuntu
            context['linux_version'] = platform.linux_distribution()[1]   # 14.04
            context['release'] = ' '.join([context['linux_dist'], context['linux_version']])
        elif context['system'] == 'Windows':                      # Windows
            context['windows_version'] = platform.win32_ver()[0]  # 7
            context['windows_build'] = platform.win32_ver()[1]    # 6.1.7601
            context['windows_csd'] = platform.win32_ver()[2]      # SP1
            context['release'] = ' '.join([context['windows_version'], context['windows_csd'], context['windows_build']])
        elif context['system'] == 'Java':
            # ...
            pass
        else:
            context['mac_release'] = platform.mac_ver()[0]
            context['mac_version'] = platform.mac_ver()[1][0]

        def _samtools_version():
            samtools = os.path.join(SAMTOOLS_FOLDER, 'samtools')
            cmd = '{samtools} --help'.format(samtools=samtools)
            #cmd = 'samtools --help'
            out = str(sp.check_output(cmd, shell=True), 'utf-8')
            pattern = re.compile('.*Version: ([\d\.]+) .*', re.DOTALL)
            return re.match(pattern, out).group(1)

        def _picard_version():
            try:
                cmd = '{java} -jar {picard}/picard.jar ViewSam --version'.format(java=JAVA, picard=PICARD_FOLDER)
                out = ''
                try:
                    out = str(sp.check_output(cmd, shell=True, stderr=sp.STDOUT), 'utf-8')
                except sp.CalledProcessError as e:
                    out = str(e.output, 'utf-8')
                pattern = re.compile('([\d\.]+)\(.*', re.DOTALL)
                return re.match(pattern, out).group(1)
            except:
                return 'Not used'

        def _gatk_version():
            try:
                cmd = '{java} -jar {gatk}/GenomeAnalysisTK.jar --version'.format(java=JAVA, gatk=GATK_FOLDER)
                out = ''
                try:
                    out = str(sp.check_output(cmd, shell=True, stderr=sp.STDOUT), 'utf-8')
                except sp.CalledProcessError as e:
                    out = str(e.output, 'utf-8')
                return out.strip()
            except:
                return 'Not used'

        context['samtools_version'] = _samtools_version()
        context['picard_version'] = _picard_version()
        context['gatk_version'] = _gatk_version()

        [called, silent, aachange, noorf, uncalled, nocov, samplenames] = count_snps.run(TABLE_COMPLETE)

        context['called'] = called
        context['silent'] = silent
        context['aachange'] = aachange
        context['noorf'] = noorf
        context['uncalled'] = uncalled
        context['nocov'] = nocov
        context['samplenames'] = samplenames

        template_dir = workflow.basedir
        template_file = 'report.tex'
        loader = jinja2.FileSystemLoader(template_dir)
        env = jinja2.Environment(loader=loader)
        template = env.get_template(template_file)

        pdf_latex = template.render(pipeline=context)

        with open(REPORT, 'w') as latex:
            latex.write(pdf_latex)

        process = sp.Popen(['pdflatex', '-interaction=nonstopmode', '-output-directory={}'.format(OUTPUT), REPORT], stdout = sp.DEVNULL, stderr = sp.DEVNULL)
        #for line in iter(process.stderr.readline, b''):
        #    print(line)

        process.communicate()
