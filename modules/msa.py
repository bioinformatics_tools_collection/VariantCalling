#! /usr/bin/python3
import pandas as pd
import argparse
import math
import pysam
import re
import numpy as np
from Bio.Seq import MutableSeq
from Bio import SeqIO

# AT => W
# CG => S
# AC => M
# GT => K
# AG => R
# CT => Y

encoding = {
        'A/A' : 'A',
        'A/C' : 'M',
        'A/G' : 'R',
        'A/T' : 'W',
        'C/A' : 'M',
        'C/C' : 'C',
        'C/G' : 'S',
        'C/T' : 'Y',
        'G/A' : 'R',
        'G/C' : 'S',
        'G/G' : 'G',
        'G/T' : 'K',
        'T/A' : 'W',
        'T/C' : 'Y',
        'T/G' : 'K',
        'T/T' : 'T'}

def get_readgroup_idsm(bamfile):
    header = pysam.view('-H', bamfile, catch_stdout=True, split_lines=True)
    temp_dict = dict()
    smid_dict = dict()
    for line in header:
        if line.startswith('@RG'):
            frags = line.split('\t')[1:]
            for frag in frags:
                key, value = frag.split(':')
                temp_dict[key] = value
            smid_dict[temp_dict['ID']] = temp_dict['SM']
            temp_dict = dict()
    return smid_dict

def get_sample_dict(table):
    sample_pattern = re.compile('^#\s*(S\d*)\s*:\s*(.*)')
    samplenames = dict()
    with open(table, 'r') as f:
        for line in f:
            try:
                [identifier, sample] = re.match(sample_pattern, line).groups()
                samplenames[identifier] = sample
            except:
                continue
    return samplenames

def insert_sequence(mutableSeq, pos, sequence):
    for i, s in enumerate(sequence):
        mutableSeq.insert(pos+i, s)

def run(snp, ref, bam, out, ambig, majority):

    parser = argparse.ArgumentParser()
    parser.add_argument('complete')
    parser.add_argument('reference')
    parser.add_argument('mapping')
    parser.add_argument('output')
    parser.add_argument('ambiguous_cutoff')
    parser.add_argument('--majority', default=False, action='store_true')
    args,uargs = parser.parse_known_args([str(snp), str(ref), str(bam), str(out), str(ambig), str(majority)])
    args.ambiguous_cutoff = ambig

    df = pd.read_table(args.complete, comment='#')
    ncol = len(df.columns)
    nsamples = (len(df.columns) - 15) // 5
    samplenames = get_sample_dict(args.complete)
    ref_length = 0

    sequences = list()
    if args.reference.endswith('.fa') or args.reference.endswith('.fasta'):
        t = "fasta"
    else:
        t = "genbank"
    for i in range(nsamples):
        sequences.append(SeqIO.read(open(args.reference), t).seq.tomutable())
    ref_length = len(sequences[0])

    reference = SeqIO.read(open(args.reference), t).seq.tomutable()
    reference_id =  SeqIO.read(open(args.reference), t).id

    if args.mapping.endswith('.sam'):
        pysam.view('-h', args.mapping, '-o', args.mapping[:-3] + 'bam', catch_stdout=False)
        args.mapping = args.mapping[:-3] + 'bam'
    pysam.index(args.mapping)
    bamfile = pysam.AlignmentFile(args.mapping, 'rb')
    id_to_sm = get_readgroup_idsm(args.mapping)

    name_to_sequence = dict()
    i = 0

    for key, sample in samplenames.items():
        coverage = np.zeros(ref_length, np.int)
        for alignment in bamfile.fetch(multiple_iterators=True):
            if id_to_sm[alignment.get_tag('RG')] == sample:
                coverage[alignment.reference_start:alignment.reference_end] += 1
        for x in np.where(coverage == 0)[0]:
            sequences[i][int(x)] = 'N'
        name_to_sequence[sample] = i
        i += 1
    sequence_to_name = {v: k for k, v in name_to_sequence.items()}

    j = 0
    offset = 0 #when a insertion occurs, we have to adjust the positions for all following snps
    for name, snp in df.iterrows():
        snps = dict()
        pos = int(snp['POS'])
        reflen = len(str(snp['REF']))
        max_insert_length = 0
       
        for i in range(nsamples):
            snps['S{}'.format(i+1)] = {
                    'GT' : snp['S{}.GT'.format(i+1)],
                    'COV' : int(snp['S{}.COV'.format(i+1)]) if not math.isnan(snp['S{}.COV'.format(i+1)]) else -1 ,
                    'COVA' : int(snp['S{}.COVA'.format(i+1)]) if not math.isnan(snp['S{}.COVA'.format(i+1)]) else -1 ,
                    'COVR' : int(snp['S{}.COVR'.format(i+1)]) if not math.isnan(snp['S{}.COVR'.format(i+1)]) else -1 ,
                    }
        for key, value in snps.items():
            if '.' in value['GT']:
                value['GT'] = value['GT'].replace('.', 'N')
            if '/' in value['GT']:
                frags = value['GT'].split('/')
                if len(frags[0]) == len(frags[1]):
                    if not args.majority:
                        if min(value['COVA'], value['COVR'])/(value['COVA'] + value['COVR']) > args.ambiguous_cutoff/100:
                            snps[key]['GT'] = encoding[value['GT']]
                        elif min(value['COVA'], value['COVR']) == value['COVR']:
                            snps[key]['GT'] = frags[0]
                        else:
                            snps[key]['GT'] = frags[1]
                    else:
                        if snps[key]['COVR'] == snps[key]['COVA']:
                            snps[key]['GT'] = encoding[value['GT']]
                        else:
                            snps[key]['GT'] = frags[0] if snps[key]['COVR'] >= snps[key]['COVA'] else frags[1]
                else:
                    snps[key]['GT'] = frags[0] if snps[key]['COVR'] >= snps[key]['COVA'] else frags[1]
            max_insert_length = max(max_insert_length, len(snps[key]['GT']))

        #print('max insert length is: {}'.format(max_insert_length))
        #print('reflen is: {}'.format(reflen))

        #Note: max_insert_length - 1 is the maximal insertion length since the first base is an unaltered one
        for key,value in snps.items():
            if '.' in value['GT']:
                value['GT'] = value['GT'].replace('.', 'N')
            #print('we are in sample {}. the alt is {}'.format(samplenames[key], value['GT']))
            if value['COV'] == -1 and reflen - max_insert_length > 1: #fill up insertion in another sequence even if there is no coverage
                insert_sequence(sequences[name_to_sequence[samplenames[key]]], pos+offset, 'N' * (max_insert_length - 1))
                #print('we have no coverage in sample {}'.format(samplenames[key]))
            if len(value['GT']) < reflen: #deletion
                for p in range(pos+offset+len(value['GT'])-1, pos+offset+reflen-1):
                    sequences[name_to_sequence[samplenames[key]]][p] = 'N'
                #print('we have a deletion in sample {}. Filling up {} to {} with N.'.format(samplenames[key], pos+offset+len(value['GT']), pos+offset+reflen))
                if max_insert_length - reflen >= 1:
                    insert_sequence(sequences[name_to_sequence[samplenames[key]]], pos+offset+len(value['GT'])-1, 'N' * (max_insert_length-reflen))
                    #print('we have a deletion in sample {} and have to fill up for insertion. start {}, length {}'.format(samplenames[key], pos+offset+len(value['GT']), max_insert_length-len(value['GT'])))
            elif len(value['GT']) > reflen: #insertion
                insert = value['GT'][reflen:]+'N'*(max_insert_length-len(value['GT'])) # no need for max_insert_length-1 because both insertion have the first unaltered base
                insert_sequence(sequences[name_to_sequence[samplenames[key]]], pos+offset, insert)
                #print('we have an insertion in sample {}, we are inserting {} into {}'.format(samplenames[key], insert, pos+offset))
            else: #snp
                for i, p in enumerate(range(pos+offset, pos+offset + len(value['GT']))):
                    sequences[name_to_sequence[samplenames[key]]][p-1] = value['GT'][i]
                #print('we have a snp in sample {}. Altering {} to {} to {}'.format(samplenames[key], pos+offset, pos+offset+len(value['GT']), value['GT']))
                if max_insert_length - len(value['GT']) >= 1:
                    insert_sequence(sequences[name_to_sequence[samplenames[key]]], pos+offset, 'N' * (max_insert_length-len(value['GT'])))
                    #print('we have a snp in sample {} and have to fill up for insertion. start {}, length {}'.format(samplenames[key], pos+offset+1, max_insert_length - len(value['GT'])))
        if max_insert_length - reflen > 0: # insert gaps into reference
            insert_sequence(reference, pos+offset, '-' * (max_insert_length-reflen))
        offset += max_insert_length - reflen
        #print('offset is {}'.format(offset))
        j+=1

    with open(args.output, 'w') as f:
        for i, sequence in enumerate(sequences):
            f.write('>{}\n'.format(sequence_to_name[i]))
            f.write('{}\n'.format(sequences[i]))
    with open(args.output[:-6]+'_reference.fasta', 'w') as f:
        f.write('>{}\n'.format(reference_id))
        f.write('{}'.format(reference))

if __name__ == "__main__":
    import sys
    try:
        run(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
    except:
        run(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
